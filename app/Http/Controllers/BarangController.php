<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BarangController extends Controller
{
    public function index()
    {
        $barang = DB::table('barang')->paginate(15);

        return view('cari', ['barang' => $barang]);
    }

    public function cari(Request $request)
    {
        $cari = $request->cari;

        $barang = DB::table('barang')->where('nama_barang', 'like', "%" . $cari . "%")->paginate(15);

        return view('cari', ['barang' => $barang]);
    }
}
