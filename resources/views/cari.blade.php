<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cari</title>
    
</head>
<body>
    <p>Cari Barang :</p>
	<form action="/barang/cari" method="GET">
		<input type="text" name="cari" placeholder="Nama Barang" value="{{ old('cari') }}" autocomplete="off">
		<input type="submit" value="Go">
	</form>
		
	<br/>
	<table>
		<tr>
			<th>Nama Barang</th>
			<th>Harga Barang</th>
			<th>Jumlah Barang</th>
            <th>Jenis Barang</th>
		</tr>
		@foreach($barang as $b)
		<tr>
			<td>{{ $b->nama_barang }}</td>
			<td>{{ $b->harga_barang }}</td>
			<td>{{ $b->jumlah_barang }}</td>
            <td>{{ $b->jenis_barang }}</td>
            
		</tr>
		@endforeach
	</table>

</body>
</html>